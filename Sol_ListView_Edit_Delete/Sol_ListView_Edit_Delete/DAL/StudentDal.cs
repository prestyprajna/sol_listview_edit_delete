﻿using Sol_ListView_Edit_Delete.DAL.ORD;
using Sol_ListView_Edit_Delete.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ListView_Edit_Delete.DAL
{
    public class StudentDal
    {
        #region  declaration
        private StudentDCDataContext _dc = null;
        #endregion

        #region  constructor
        public StudentDal()
        {
            _dc = new StudentDCDataContext();
        }
        #endregion

        #region  public methods

        private async Task<IEnumerable<StudentEntity>> GetStudentData()
        {
            return await Task.Run(() =>
            {
                var getData =
                _dc?.tblUsers
                ?.AsEnumerable()
                ?.Select((leUserObj) => new StudentEntity()
                {
                    StudentId = leUserObj?.StudentId,
                    FirstName = leUserObj?.FirstName,
                    LastName = leUserObj?.LastName
                })
                ?.ToList();

                return getData;
            });
        }

        public async Task<int?> GetStudentDataCount()
        {
            return await Task.Run(async() =>
            {
                var getData =
                (await this.GetStudentData())
                ?.AsEnumerable()
                ?.Count();

                return getData;
            });
        }

        public async Task<IEnumerable<StudentEntity>> GetStudentDataPagination(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async () =>
            {
                var getData =
                (await this.GetStudentData())
                ?.AsEnumerable()
                ?.Skip(startRowIndex)
                ?.Take(maximumRows)
                ?.ToList();

                return getData;
            });
        }

        public async Task<Boolean> UpdateData(StudentEntity studentEntityObj)
        {
            return await Task.Run(() =>
            {
                int? status = null;
                string message = null;

                var setQuery =
                _dc?.uspSetUser(
                    "UPDATE",
                    studentEntityObj?.StudentId,
                    studentEntityObj?.FirstName,
                    studentEntityObj?.LastName,
                    ref status,
                    ref message
                    );

                return (status == 1) ? true : false;
            });
        }

        public async Task<Boolean> DeleteData(StudentEntity studentEntityObj)
        {
            return await Task.Run(() =>
            {
                int? status = null;
                string message = null;

                var setQuery =
                _dc?.uspSetUser(
                    "DELETE",
                    studentEntityObj?.StudentId,
                    studentEntityObj?.FirstName,
                    studentEntityObj?.LastName,
                    ref status,
                    ref message
                    );

                return (status == 1) ? true : false;
            });
        }

        public async Task<StudentEntity> FindStudentData(int id)
        {
            return await Task.Run(async() =>
            {
                var setQuery =
               (await this.GetStudentData())
               ?.AsEnumerable()
               ?.FirstOrDefault((leStudentEntityObj) => leStudentEntityObj.StudentId == id);

                return setQuery;
            });
        }



        #endregion
    }
}