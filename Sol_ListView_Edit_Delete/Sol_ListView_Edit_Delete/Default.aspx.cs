﻿using Sol_ListView_Edit_Delete.DAL;
using Sol_ListView_Edit_Delete.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ListView_Edit_Delete
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        //public IQueryable<Sol_ListView_Edit_Delete.Models.StudentEntity> lvStudent_GetData()
        //{
        //    return null;
        //}

        public async Task<SelectResult> lvStudent_GetData(int startRowIndex,int maximumRows)
        {
            return await Task.Run(async() =>
            {
                int totalRowCount = Convert.ToInt32(await new StudentDal().GetStudentDataCount());

                IEnumerable<StudentEntity> getData = await new StudentDal().GetStudentDataPagination(startRowIndex, maximumRows);
                
                return new SelectResult(totalRowCount,getData);


            });
        }

        protected  void lvStudent_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvStudent.EditIndex =  e.NewEditIndex;
        }

        protected void lvStudent_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            lvStudent.EditIndex = -1;
        }

        // The id parameter name should match the DataKeyNames value set on the control
        public async Task lvStudent_UpdateItem(int StudentId)
        {
            Sol_ListView_Edit_Delete.Models.StudentEntity item = null;
            // Load the item here, e.g. item = MyDataLayer.Find(id);

            item = await new StudentDal().FindStudentData(StudentId);

            if (item == null)
            {
                // The item wasn't found
                ModelState.AddModelError("", String.Format("Item with id {0} was not found", StudentId));
                return;
            }
            TryUpdateModel(item);   //takes the new value from browser for update 
            if (ModelState.IsValid)
            {
                // Save changes here, e.g. MyDataLayer.SaveChanges();
                StudentEntity studentEntityObj = new StudentEntity()
                {
                    StudentId = item.StudentId,
                    FirstName = item.FirstName,
                    LastName = item.LastName
                };

                var flag = await new StudentDal().UpdateData(studentEntityObj);

                //if(flag==true)
                //{
                //    lvStudent.EditIndex = -1;
                //}

            }
        }

        // The id parameter name should match the DataKeyNames value set on the control
        public async Task lvStudent_DeleteItem(int StudentId) // id name should match to that of datakey name as given earlier in properties.....
        {
            StudentEntity studentEntityObj = new StudentEntity()
            {
                StudentId = StudentId
            };

            var flag = await new StudentDal().DeleteData(studentEntityObj);
        }
    }
}