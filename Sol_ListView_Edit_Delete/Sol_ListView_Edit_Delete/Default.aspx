﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_ListView_Edit_Delete.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        table{
            width:50%;
            margin:auto;
             border-collapse:collapse;
        }

        th,td {
            border:1px;
            border-style:solid;
            border-color:gray;
            text-align:center;
            padding:20px;
            margin:10px;
        }

        tr:nth-child(even){
            background-color:white;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>

                <asp:ListView ID="lvStudent" runat="server" DataKeyNames="StudentId" ItemType="Sol_ListView_Edit_Delete.Models.StudentEntity" SelectMethod="lvStudent_GetData" OnItemEditing="lvStudent_ItemEditing" OnItemCanceling="lvStudent_ItemCanceling" UpdateMethod="lvStudent_UpdateItem" DeleteMethod="lvStudent_DeleteItem">

                    <LayoutTemplate>

                        <table>

                            <thead>
                                <tr>
                                     <th>
                                        Operation
                                    </th>
                                    <th>
                                        First Name
                                    </th>
                                    <th>
                                        Last Name
                                    </th>
                                </tr>                               
                            </thead>

                            <tbody>
                                <asp:PlaceHolder ID="itemPlaceholder" runat="server">

                                </asp:PlaceHolder>
                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataPager ID="dp" runat="server" PageSize="2" PagedControlID="lvStudent">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true" /> 
                                                <asp:NumericPagerField ButtonType="Link" />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                                            </Fields>
                                        </asp:DataPager>
                                    </td>
                                </tr>
                            </tfoot>
                            
                        </table>

                    </LayoutTemplate>

                    <ItemTemplate>  
                                             
                            <tr>
                                <td>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" />
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" />
                                </td>
                                <td>
                                    <asp:Label ID="lblFirstName" runat="server" Text='<%#Item.FirstName %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblLastName" runat="server" Text='<%#Item.LastName %>'></asp:Label>
                                </td>
                            </tr>                     

                    </ItemTemplate>

                    <EditItemTemplate>
                        <tr>
                            <td>
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server" Text='<%# BindItem.FirstName %>'></asp:TextBox>
                            </td>
                             <td>
                                <asp:TextBox ID="txtLastName" runat="server" Text='<%# BindItem.LastName %>'></asp:TextBox>
                            </td>
                        </tr>
                    </EditItemTemplate>

                    <EmptyDataTemplate>
                        <span>
                            NO RECORDS FOUND
                        </span>

                    </EmptyDataTemplate>

                </asp:ListView>

            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
