﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_ListView_Edit_Delete.Models
{
    public class StudentEntity
    {
        public decimal? StudentId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}